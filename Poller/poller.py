import schedule
import time
import requests


def get_bird_data(pg_num):
    params = {"page_number": pg_num}
    api_endpoint = "http://fastapi:8000/api/birds/populate"

    try:
        response = requests.get(api_endpoint, params)

        if response.status_code == 200:
            print("Bird data retrieved successfully.")
        else:
            print(
                "Failed to get bird data. Status code: " + response.status_code
            )
    except Exception as e:
        print(f"An error occurred: {e}")


if __name__ == "__main__":
    for i in range(1, 5):
        schedule.every().day.do(get_bird_data, i)

    while True:
        schedule.run_pending()
        time.sleep(1)
