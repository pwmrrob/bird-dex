import os
from fastapi import Depends
from jwtdown_fastapi.authentication import Authenticator
from queries.users import UsersRepo, UserOutWithPassword


class UserAuth(Authenticator):
    async def get_account_data(
        self,
        email: str,
        users: UsersRepo,
    ):
        # Use your repo to get the user based on the
        # username (which could be an email)
        return users.get(email)

    def get_account_getter(
        self,
        users: UsersRepo = Depends(),
    ):
        # Return the users. That's it.
        return users

    def get_hashed_password(self, user: UserOutWithPassword):
        # Return the encrypted password value from your
        # user object
        try:
            return user.hash_password
        except AttributeError:
            return None


SIGNING_KEY = os.environ.get("SIGNING_KEY")
if not SIGNING_KEY:
    raise ValueError("SIGNING_KEY environment variable not defined")

authenticator = UserAuth(SIGNING_KEY)
