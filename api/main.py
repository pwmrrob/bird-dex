from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from authenticator import authenticator
from routers.users import router as users_router
from routers.birds import router as birds_router
from routers.sightings import router as sightings_router
from routers.ebirds_api import router as ebirds_obs_router
from seeder import seed_data
import os

app = FastAPI()
app.include_router(authenticator.router, tags=["Users"])
app.include_router(users_router, tags=["Users"])
app.include_router(birds_router, tags=["Birds"])
app.include_router(sightings_router, tags=["Sightings"])
app.include_router(ebirds_obs_router, tags=["eBird"])


@app.on_event("startup")
def startup_event():
    seed_data()


app.add_middleware(
    CORSMiddleware,
    allow_origins=[os.environ.get("CORS_HOST")],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/api/launch-details")
def launch_details():
    return {
        "launch_details": {
            "module": 3,
            "week": 17,
            "day": 5,
            "hour": 19,
            "min": "00",
        }
    }
