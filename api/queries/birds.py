from models import BirdIn, BirdOut
from queries.pool import pool
from acl import nuthatch


class BirdsRepo:
    def get_bird_by_nametype(self, name_type: str, name: str):
        if name_type == "common_name":
            name = name.replace("'", "''")
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT *
                        FROM birds
                        WHERE {} = '{}'
                        """.format(
                            name_type, name
                        )
                    )
                    dataset = db.fetchall()
                    birds = [
                        BirdOut(
                            id=data[0],
                            common_name=data[1],
                            sci_name=data[2],
                            conservation_status=data[3],
                            image=data[4],
                        )
                        for data in dataset
                    ]

                    return birds
        except Exception as e:
            return {"message": "error " + str(e)}

    def create(self, info: BirdIn) -> BirdOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        INSERT INTO birds
                            (common_name, sci_name, conservation_status, image)
                        VALUES (%s, %s, %s, %s)
                        ON CONFLICT (common_name) DO NOTHING
                        RETURNING
                            id,
                            common_name,
                            sci_name,
                            conservation_status,
                            image
                        """,
                        [
                            info.common_name,
                            info.sci_name,
                            info.conservation_status,
                            info.image,
                        ],
                    )
                    id = db.fetchone()[0]
                    return BirdOut(
                        id=id,
                        common_name=info.common_name,
                        sci_name=info.sci_name,
                        conservation_status=info.conservation_status,
                        image=info.image,
                    )
        except Exception as e:
            return {"message": "error " + str(e)}

    def populate_table(self, page_number: int = 1) -> bool:
        # get a list of birds and add each to the database
        birds = nuthatch.get_birds(page_number)

        for bird in birds:
            self.create(bird)

        return True
