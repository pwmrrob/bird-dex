from models import SightingIn, SightingInBase, SightingOut, BirdOut, UserOut
from queries.pool import pool
from acl.geocode import get_location


class SightingsRepo:
    def create(self, info: SightingIn) -> SightingOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    lat, lng = get_location(info.location)
                    sql = """
                        INSERT INTO sightings
                            (date, location, lat, lng, image, user_id, bird_id)
                        VALUES ('{}', '{}', {}, {}, '{}', {}, {})
                        RETURNING
                            id
                        """.format(
                        info.date,
                        info.location,
                        lat,
                        lng,
                        info.image,
                        info.user_id,
                        info.bird_id,
                    )
                    conn.autocommit = False
                    db.execute(sql)
                    conn.commit()
                    sighting_data = db.fetchone()
                    data = self.get(
                        info.user_id, "sightings.id", sighting_data[0]
                    )[0].dict()
                    sighting = SightingOut(**data)
                    return sighting
        except Exception as e:
            return {"message": str(e)}
        finally:
            conn.autocommit = True

    def get(
        self, user_id: int, name_type: str, common_name: str
    ) -> SightingOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                            SELECT
                                sightings.id,
                                sightings.date,
                                sightings.location,
                                sightings.lat,
                                sightings.lng,
                                sightings.image,
                                users.id,
                                users.email,
                                users.first_name,
                                users.last_name,
                                users.location,
                                users.lat,
                                users.lng,
                                birds.id,
                                birds.common_name,
                                birds.sci_name,
                                birds.conservation_status,
                                birds.image
                            FROM
                                sightings
                            JOIN
                                birds ON sightings.bird_id = birds.id
                            JOIN
                                users ON sightings.user_id = users.id
                            WHERE
                                sightings.user_id = {} AND {} = {};
                            """.format(
                            user_id, name_type, common_name
                        )
                    )
                    dataset = db.fetchall()
                    sightings = [
                        SightingOut(
                            id=data[0],
                            date=data[1],
                            location=data[2],
                            lat=data[3],
                            lng=data[4],
                            image=data[5],
                            user=UserOut(
                                id=data[6],
                                email=data[7],
                                first_name=data[8],
                                last_name=data[9],
                                location=data[10],
                                lat=data[11],
                                lng=data[12],
                            ),
                            bird=BirdOut(
                                id=data[13],
                                common_name=data[14],
                                sci_name=data[15],
                                conservation_status=data[16],
                                image=data[17],
                            ),
                        )
                        for data in dataset
                    ]
                    return sightings
        except Exception as e:
            return {"message": str(e)}

    def update(
        self, user_id: int, id: int, info: SightingInBase
    ) -> SightingOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    lat, lng = get_location(info.location)
                    conn.autocommit = False
                    db.execute(
                        """
                        UPDATE sightings
                        SET
                            date = '{}',
                            location = '{}',
                            lat = {},
                            lng = {},
                            image = '{}',
                            bird_id = {}
                        WHERE sightings.id = {}
                        """.format(
                            info.date,
                            info.location,
                            lat,
                            lng,
                            info.image,
                            info.bird_id,
                            id,
                        )
                    )
                    conn.commit()
                    data = self.get(user_id, "sightings.id", id)[0].dict()
                    sighting = SightingOut(**data)
                    return sighting
        except Exception as e:
            print({"message": str(e)})
            return True
        finally:
            conn.autocommit = True

    def delete(self, id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM sightings
                        WHERE id = {};
                        """.format(
                            id
                        )
                    )
                    return True
        except Exception as e:
            return {"message": str(e)}
