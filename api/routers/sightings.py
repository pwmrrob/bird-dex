from typing import Union, List
from fastapi import (
    Depends,
    HTTPException,
    status,
    Response,
    APIRouter,
    Request,
)
from queries.sightings import SightingsRepo
from models import HttpError, SightingIn, SightingOut, SightingInBase

router = APIRouter()


# Create Sighting for user
@router.post(
    "/api/users/{user_id}/sightings/", response_model=SightingOut | HttpError
)
async def create_sighting(
    info: SightingInBase,
    request: Request,
    response: Response,
    user_id: int,
    repo: SightingsRepo = Depends(),
) -> SightingOut:
    try:
        sighting = repo.create(SightingIn(user_id=user_id, **info.dict()))
    except (Exception, BaseException) as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"Cannot create sighting. Exception: {str(e)}",
        )
    return sighting


# Get Sightings for user
@router.get(
    "/api/users/{user_id}/sightings",
    response_model=Union[List[SightingOut], SightingOut, None],
)
async def get_sighting_by_user(
    request: Request,
    response: Response,
    user_id: int,
    common_name: str | None = None,
    repo: SightingsRepo = Depends(),
) -> Union[List[SightingOut], SightingOut, None]:
    try:
        if common_name:
            sightings = repo.get(
                user_id, "birds.common_name", f"'{common_name}'"
            )
        else:
            sightings = repo.get(user_id, "'1'", "1")
    except (Exception, BaseException) as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"Cannot list sighting. Exception: {str(e)}",
        )
    return sightings


# Get sighting details
@router.get(
    "/api/users/{user_id}/sightings/{id}",
    response_model=Union[List[SightingOut], SightingOut, None],
)
async def get_sighting_details(
    request: Request,
    response: Response,
    user_id: int,
    id: int,
    repo: SightingsRepo = Depends(),
) -> Union[List[SightingOut], SightingOut, None]:
    try:
        sighting = repo.get(user_id, "sightings.id", id)
    except (Exception, BaseException) as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"Cannot list sighting. Exception: {str(e)}",
        )
    return sighting


# Update sighting details
@router.put("/api/users/{user_id}/sightings/{id}", response_model=SightingOut)
async def update_sighting(
    info: SightingInBase,
    request: Request,
    response: Response,
    user_id: int,
    id: int,
    repo: SightingsRepo = Depends(),
) -> SightingOut:
    try:
        sighting = repo.update(user_id, id, info)
    except (Exception, BaseException) as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"Cannot update sighting. Exception: {str(e)}",
        )
    return sighting


# Delete sighting
@router.delete("/api/users/{user_id}/sightings/{id}", response_model=bool)
async def delete_sighting(
    request: Request,
    response: Response,
    user_id: int,
    id: int,
    repo: SightingsRepo = Depends(),
) -> bool:
    return repo.delete(id)
