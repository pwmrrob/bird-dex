# router.py
from typing import Union, List
from queries.sightings import SightingsRepo

from fastapi import (
    Depends,
    HTTPException,
    status,
    Response,
    APIRouter,
    Request,
)

from queries.birds import BirdsRepo
from authenticator import authenticator

from models import (
    BirdSighting,
    UserOut,
    HttpError,
    BirdIn,
    BirdOut,
)

router = APIRouter()


# Create Bird
@router.post("/api/birds", response_model=BirdOut | HttpError)
async def create_bird(
    info: BirdIn,
    request: Request,
    response: Response,
    repo: BirdsRepo = Depends(),
) -> BirdOut:
    try:
        bird = repo.create(info)
    except (Exception, BaseException) as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"Cannot create bird. Exception: {str(e)}",
        )
    return bird


# Get Bird(s) by common_name or sci_name via query params
#   No query params lists all birds
@router.get("/api/birds", response_model=Union[List[BirdOut], BirdOut, None])
async def get_bird_by_nametype(
    request: Request,
    response: Response,
    user: UserOut = Depends(authenticator.try_get_current_account_data),
    repo: BirdsRepo = Depends(),
    sightingsRepo: SightingsRepo = Depends(),
    sci_name: str | None = None,
    common_name: str | None = None,
) -> Union[List[BirdOut], BirdOut, None]:
    try:
        if common_name:
            birds = repo.get_bird_by_nametype("common_name", common_name)
        elif sci_name:
            birds = repo.get_bird_by_nametype("sci_name", sci_name)
        else:
            birds = repo.get_bird_by_nametype("'1'", "1")
        if user:
            sightings = sightingsRepo.get(user["id"], "'1'", "1")
        else:
            sightings = []
        for bird in birds:
            bird.sightings = [
                BirdSighting(**sighting.dict())
                for sighting in sightings
                if sighting.bird.id == bird.id
            ]
    except (Exception, BaseException) as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"Cannot list bird. Exception: {str(e)}",
        )

    return birds


# Populates Birds table with data pulled from Nuthatch
@router.get("/api/birds/populate", response_model=bool)
async def populate_table(
    request: Request, repo: BirdsRepo = Depends(), page_number: int = 1
) -> bool:
    try:
        repo.populate_table(page_number)
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Cannot populate birds table. Exception: " + str(e),
        )

    return True
