import unittest
from unittest.mock import patch, Mock
from acl.ebirds import get_obs
from models import Observation
import os
import json


class FakeBirdsRepo:
    # Various cases of database state
    birds = []
    two_birds = [
        Observation(
            common_name="American Crow",
            sci_name="Turdus migratorius",
            location="Miami, FL",
            image="example.com/image.jpg",
        ),
        Observation(
            common_name="American Robin",
            sci_name="Turdus migratorius",
            location="Miami, FL",
            image="example.com/image.jpg",
        ),
    ]
    three_birds = [
        Observation(
            common_name="American Crow",
            sci_name="Turdus migratorius",
            location="Miami, FL",
            image="example.com/image.jpg",
        ),
        Observation(
            common_name="American Robin",
            sci_name="Turdus migratorius",
            location="Miami, FL",
            image="example.com/image.jpg",
        ),
        Observation(
            common_name="European Robin",
            sci_name="Turdus migratorius",
            location="Miami, FL",
            image="example.com/image.jpg",
        ),
    ]

    # set state of the database
    def setBirds(self, set: int):
        match set:
            case 0:
                self.birds = []
            case 2:
                self.birds = self.two_birds
            case 3:
                self.birds = self.three_birds

    # Mock the database
    def get_bird_by_nametype(self, name_type, name):
        namedbirds = []
        for bird in self.birds:
            if bird.common_name == name:
                namedbirds.append(bird)
        return namedbirds


class TestGetObs(unittest.TestCase):
    @patch("requests.get")
    def test_get_obs_nearby_with_two_birds_no_images(self, mock_get):
        # Arrange
        mock_response = Mock()
        expected_result = [
            Observation(
                common_name="American Robin",
                sci_name="Turdus migratorius",
                location="Miami, FL",
                image=None,
            ),
            Observation(
                common_name="American Robin",
                sci_name="Turdus migratorius",
                location="Miami, FL",
                image=None,
            ),
        ]  # This is what the function should return

        repo = FakeBirdsRepo()  # This is the repo we're testing
        repo.setBirds(0)

        mock_response.status_code = 200
        mock_response.text = json.dumps(
            [
                {
                    "comName": "American Robin",
                    "sciName": "Turdus migratorius",
                    "locName": "Miami, FL",
                },
                {
                    "comName": "American Robin",
                    "sciName": "Turdus migratorius",
                    "locName": "Miami, FL",
                },
            ]
        )  # This is what the API should returns
        mock_get.return_value = mock_response

        # Act
        result = get_obs(repo, (1.0, 1.0))

        # Assert
        mock_get.assert_called_once_with(
            "https://api.ebird.org/v2/data/obs/geo/recent",
            headers={"X-eBirdApiToken": os.environ.get("EBIRDS_API_KEY")},
            params={"lat": 1.0, "lng": 1.0, "maxResults": 100},
        )
        self.assertEqual(result, expected_result)

    @patch("requests.get")
    def test_get_obs_nearby_with_three_birds_two_images(self, mock_get):
        # Arrange
        mock_response = Mock()
        expected_result = [
            Observation(
                common_name="American Crow",
                sci_name="Turdus migratorius",
                location="Miami, FL",
                image="example.com/image.jpg",
            ),
            Observation(
                common_name="American Robin",
                sci_name="Turdus migratorius",
                location="Miami, FL",
                image="example.com/image.jpg",
            ),
            Observation(
                common_name="European Robin",
                sci_name="Turdus migratorius",
                location="Miami, FL",
                image=None,
            ),
        ]  # This is what the function should return

        repo = FakeBirdsRepo()  # This is the repo we're testing
        repo.setBirds(2)

        mock_response.status_code = 200
        mock_response.text = json.dumps(
            [
                {
                    "comName": "American Crow",
                    "sciName": "Turdus migratorius",
                    "locName": "Miami, FL",
                },
                {
                    "comName": "American Robin",
                    "sciName": "Turdus migratorius",
                    "locName": "Miami, FL",
                },
                {
                    "comName": "European Robin",
                    "sciName": "Turdus migratorius",
                    "locName": "Miami, FL",
                },
            ]
        )  # This is what the API returns
        mock_get.return_value = mock_response

        # Act
        result = get_obs(repo, (1.0, 1.0))

        # Assert
        mock_get.assert_called_once_with(
            "https://api.ebird.org/v2/data/obs/geo/recent",
            headers={"X-eBirdApiToken": os.environ.get("EBIRDS_API_KEY")},
            params={"lat": 1.0, "lng": 1.0, "maxResults": 100},
        )
        for obs in expected_result:
            self.assertIn(obs, result)

    @patch("requests.get")
    def test_get_obs_US_with_three_birds_all_images(self, mock_get):
        # Arrange
        mock_response = Mock()
        expected_result = [
            Observation(
                common_name="American Crow",
                sci_name="Turdus migratorius",
                location="Miami, FL",
                image="example.com/image.jpg",
            ),
            Observation(
                common_name="American Robin",
                sci_name="Turdus migratorius",
                location="Miami, FL",
                image="example.com/image.jpg",
            ),
            Observation(
                common_name="European Robin",
                sci_name="Turdus migratorius",
                location="Miami, FL",
                image="example.com/image.jpg",
            ),
        ]  # This is what the function should return

        repo = FakeBirdsRepo()  # This is the repo we're testing
        repo.setBirds(3)

        mock_response.status_code = 200
        mock_response.text = json.dumps(
            [
                {
                    "comName": "American Crow",
                    "sciName": "Turdus migratorius",
                    "locName": "Miami, FL",
                },
                {
                    "comName": "American Robin",
                    "sciName": "Turdus migratorius",
                    "locName": "Miami, FL",
                },
                {
                    "comName": "European Robin",
                    "sciName": "Turdus migratorius",
                    "locName": "Miami, FL",
                },
            ]
        )  # This is what the API returns
        mock_get.return_value = mock_response

        # Act
        result = get_obs(repo)

        # Assert
        mock_get.assert_called_once_with(
            "https://api.ebird.org/v2/data/obs/US/recent",
            headers={"X-eBirdApiToken": os.environ.get("EBIRDS_API_KEY")},
            params={"maxResults": 50},
        )
        for obs in expected_result:
            self.assertIn(obs, result)
