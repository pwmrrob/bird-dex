from fastapi.testclient import TestClient
from authenticator import authenticator
from main import app

client = TestClient(app)


def fake_get_current_account_data():
    return {
        "id": 1,
        "email": "beowulf@geats.com",
        "first_name": "Emily",
        "last_name": "Johnson",
        "location": "Miami, FL",
        "lat": 0,
        "lng": 0,
    }


def test_get_obs_nearby():
    # Arrange
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data

    # Act
    response = client.get("/ebirds/nearby")

    # Assert
    assert response.status_code == 200

    # Clean up
    app.dependency_overrides = {}


def test_get_obs_region():
    # Act
    response = client.get("/ebirds/region")

    # Assert
    assert response.status_code == 200
