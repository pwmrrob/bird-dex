from fastapi.testclient import TestClient
from main import app
from queries.birds import BirdsRepo

client = TestClient(app)


class FakeNuthatch:
    def get_birds(self, page_number: int):
        return [
            {
                "common_name": "Golduck",
                "sci_name": "BigBlueDuck",
                "conservation_status": "LC",
                "image":
                "https://assets.pokemon.com/assets\
                    /cms2/img/pokedex/full/055.png",
            },
            {
                "common_name": "Psyduck",
                "sci_name": "CrazyDuck",
                "conservation_status": "LC",
                "image":
                "https://assets.pokemon.com/assets\
                    /cms2/img/pokedex/full/054.png",
            },
        ]

    def populate_table(self, page_num: int = None) -> bool:
        return True


def populate_fake_table(page_number: int = 1) -> bool:
    birds = FakeNuthatch.populate_table(page_number)
    return birds


def fake_get_birds(page_number: int):
    return FakeNuthatch.populate_table(page_number)


def test_populate_table():
    # Arrange
    app.dependency_overrides[BirdsRepo] = FakeNuthatch

    # Act
    response = client.get("/api/birds/populate")

    # Assert
    assert response.status_code == 200
    assert response.json()
