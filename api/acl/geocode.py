from typing import Tuple, Union, Dict
from urllib.parse import quote
import json
import requests
import os


GOOGLE_API_KEY = os.environ["GOOGLE_API_KEY"]
if not GOOGLE_API_KEY:
    raise ValueError("GOOGLE_API_KEY environment variable not defined")


def get_location(location: str) -> Union[Tuple[float, float], Dict[str, str]]:
    location = quote(location)
    geocode_url = (
        "https://maps.googleapis.com/"
        "maps/api/geocode/json?"
        f"address={location}&key={GOOGLE_API_KEY}"
    )

    try:
        geocode_response = requests.get(geocode_url)
        geocode = json.loads(geocode_response.content)

    except Exception as e:
        return {"message": f"Failed to retrieve. Error: {str(e)}"}

    try:
        lat = geocode["results"][0]["geometry"]["location"]["lat"]
        lng = geocode["results"][0]["geometry"]["location"]["lng"]
        return lat, lng

    except (KeyError, IndexError):
        # middle of the US if no location found
        return 37, -97
