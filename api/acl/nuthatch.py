import os
import requests
import json
from typing import List
from models import BirdIn, ApiError

NUTHATCH_API_KEY = os.environ["NUTHATCH_API_KEY"]
if not NUTHATCH_API_KEY:
    raise ValueError("NUTHATCH_API_KEY environment variable not defined")


def get_birds(
    page_number: int = 1, page_size: int = 100
) -> List[BirdIn] | None:

    headers = {"API-Key": NUTHATCH_API_KEY}
    base_url = "https://nuthatch.lastelm.software/v2/birds"
    params = {
        "page": page_number,
        "pageSize": page_size,
        "hasImg": True,
    }
    url = base_url + params

    # make the request and handle the response
    try:
        response = requests.get(url, headers=headers, params=params)
        if response.status_code != 200:
            return ApiError(detail=response.text)
        content = json.loads(response.text)
        birds = content["entities"]
        results = []

        # for each bird, validate the data, then add it to the results
        for bird in birds:
            if not (
                bird.get("status") and
                bird.get("images") and
                bird.get("name") and
                bird.get("sciName")
            ):
                continue
            results.append(
                BirdIn(
                    common_name=bird["name"],
                    sci_name=bird["sciName"],
                    conservation_status=bird["status"],
                    image=bird["images"][0],
                )
            )
        return results
    except Exception as e:
        return ApiError(detail=e)
