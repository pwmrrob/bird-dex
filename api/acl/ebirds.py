import requests
import os
import json
from queries.birds import BirdsRepo
import random
from models import Observation, ApiError
from typing import Tuple, List

EBIRDS_API_KEY = os.environ.get("EBIRDS_API_KEY")
if not EBIRDS_API_KEY:
    raise ValueError("EBIRDS_API_KEY environment variable not defined")


def get_obs(
    repo: BirdsRepo,
    lat_lng: Tuple[float, float] | None = None,
) -> List[Observation] | ApiError:

    headers = {"X-eBirdApiToken": EBIRDS_API_KEY}

    # set url and params depending on inputs
    if lat_lng is None:
        params = {"maxResults": 50}
        url = "https://api.ebird.org/v2/data/obs/US/recent"
    else:
        params = {"lat": lat_lng[0], "lng": lat_lng[1], "maxResults": 100}
        url = "https://api.ebird.org/v2/data/obs/geo/recent"

    # make request and handle response
    try:
        response = requests.get(url, headers=headers, params=params)
        if response.status_code == 200:
            data = json.loads(response.text)

            # get 3 random birds from the response
            obs_list = []
            backup_list = []
            i = len(data)
            while len(obs_list) < 3 and i > 0:
                obs = data.pop(random.randint(0, len(data) - 1))
                our_birds = repo.get_bird_by_nametype(
                    "common_name", obs["comName"]
                )
                if len(our_birds) > 0:
                    obs_list.append(
                        Observation(
                            common_name=obs["comName"],
                            sci_name=obs["sciName"],
                            location=obs["locName"],
                            image=our_birds[0].image,
                        )
                    )
                else:  # set it aside if it's no good
                    backup_list.append(
                        Observation(
                            common_name=obs["comName"],
                            sci_name=obs["sciName"],
                            location=obs["locName"],
                            image=None,
                        )
                    )
                i -= 1

            # If we can't find enough birds in our database, just return any 3
            while len(obs_list) < 3 and len(backup_list) > 0:
                obs_list.append(backup_list.pop())

            return obs_list
        else:
            return ApiError(detail=response.text)
    except Exception as e:
        return ApiError(detail=str(e))
