import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'

export const birdsApi = createApi({
    reducerPath: 'birddexApi',
    tagTypes: ['Birds', 'Sighting', 'User'],
    baseQuery: fetchBaseQuery({
        baseUrl: `${import.meta.env.VITE_BACKEND_HOST}`,
    }),
    endpoints: (builder) => ({
        getBirds: builder.query({
            query: () => ({
                url: '/api/birds',
                credentials: 'include',
            }),
            providesTags: ['Birds'],
        }),
        getSightings: builder.query({
            query: (user_id) => ({
                url: `/api/users/${user_id}/sightings`,
            }),
            providesTags: ['Sightings'],
        }),
        getToken: builder.query({
            query: () => ({
                url: '/token',
                credentials: 'include',
            }),
            providesTags: ['User'],
        }),
        getObservations: builder.query({
            query: () => ({
                url: `/ebirds/nearby`,
                credentials: 'include',
            }),
            providesTags: ['Observations'],
        }),
        login: builder.mutation({
            query: (info) => {
                let formData = new FormData()
                formData.append('username', info.email)
                formData.append('password', info.password)

                return {
                    url: '/token',
                    method: 'POST',
                    body: formData,
                    credentials: 'include',
                }
            },
            invalidatesTags: ['User', 'Birds'],
        }),
        logout: builder.mutation({
            query: () => ({
                url: '/token',
                method: 'DELETE',
                credentials: 'include',
            }),
            invalidatesTags: ['User', 'Birds'],
        }),
        signup: builder.mutation({
            query: (info) => {
                return {
                    url: '/api/users',
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(info),
                    credentials: 'include',
                }
            },
            invalidatesTags: (result) => {
                return (result && ['User', 'Birds']) || []
            },
        }),
        addSighting: builder.mutation({
            query: ({ userId, info }) => {
                return {
                    url: `/api/users/${userId}/sightings/`,
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(info),
                    credentials: 'include',
                }
            },
            invalidatesTags: (result) => {
                return (result && ['Sightings', 'Birds']) || []
            },
        }),
        updateSighting: builder.mutation({
            query: ({ userId, sightingId, info }) => {
                return {
                    url: `/api/users/${userId}/sightings/${sightingId}`,
                    method: 'PUT',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(info),
                    credentials: 'include',
                }
            },
            invalidatesTags: (result) => {
                return (result && ['Sightings', 'Birds']) || []
            },
        }),
        deleteSighting: builder.mutation({
            query: ({ userId, sightingId }) => {
                return {
                    url: `/api/users/${userId}/sightings/${sightingId}`,
                    method: 'DELETE',
                    credentials: 'include',
                }
            },
            invalidatesTags: ['Sightings', 'Birds'],
        }),
    }),
})

export const {
    useGetBirdsQuery,
    useGetSightingsQuery,
    useLogoutMutation,
    useLoginMutation,
    useGetTokenQuery,
    useSignupMutation,
    useAddSightingMutation,
    useUpdateSightingMutation,
    useDeleteSightingMutation,
    useLazyGetSightingsQuery,
    useLazyGetObservationsQuery,
} = birdsApi
