import { useState } from 'react'
import { useDispatch } from 'react-redux'
import { setListPosition } from './listPosition/listPositionSlice.js'
import {
    useGetTokenQuery,
    useAddSightingMutation,
    useUpdateSightingMutation,
    useDeleteSightingMutation,
} from '../Components/api/birddexApi.js'
import { setSelectedCard } from './selectedCard/selectedCardSlice.js'

function SightingForm({
    action = '',
    sightingInfo = '',
    birdInfo = '',
    toggle,
}) {
    const dispatch = useDispatch()
    // fetch logged in user data
    const { data: user } = useGetTokenQuery()
    // RTK Query mutations
    const [addSighting] = useAddSightingMutation()
    const [updateSighting] = useUpdateSightingMutation()
    const [deleteSighting] = useDeleteSightingMutation()
    // initial form data state
    const [formData, setFormData] = useState({
        date: sightingInfo.date || '',
        location: sightingInfo.location || '',
        image: sightingInfo.image || birdInfo.image,
    })

    // convert date data for JSON
    const formatDate = (date) => {
        const isoString = new Date(date).toISOString()
        return isoString.split('T')[0]
    }

    // handle form submit
    const handleSubmit = (e) => {
        e.preventDefault()
        switch (action) {
            case 'create':
                addSighting({
                    userId: user.user.id,
                    info: {
                        bird_id: birdInfo.id,
                        date: formatDate(formData.date),
                        location: formData.location,
                        image: formData.image,
                    },
                })
                break
            case 'update':
                updateSighting({
                    userId: user.user.id,
                    sightingId: sightingInfo.id,
                    info: {
                        bird_id: birdInfo.id,
                        date: formatDate(formData.date),
                        location: formData.location,
                        image: formData.image,
                    },
                })
                break
            case 'delete':
                deleteSighting({
                    userId: user.user.id,
                    sightingId: sightingInfo.id,
                })
                break
        }
        // Closes the modal
        toggle()
        // Closes the accordion so the list can update
        dispatch(setListPosition(-1))
        dispatch(setSelectedCard(-1))
    }

    // updates form data state
    const handleChange = (e) => {
        const value = e.target.value
        const inputName = e.target.name
        setFormData({
            ...formData,
            [inputName]: value,
        })
    }
    return (
        <div className="row">
            <div className="offset-1 col-10">
                <div className="shadow p-4 mt-4 mb-4">
                    <form onSubmit={handleSubmit} id="create-sighting-form">
                        <div className="form-floating mb-3">
                            <input
                                onChange={handleChange}
                                value={formData.date}
                                placeholder="Date"
                                required
                                type="date"
                                name="date"
                                id="date"
                                className="form-control"
                                readOnly={action === 'delete'}
                            />
                            <label htmlFor="date">Date</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                onChange={handleChange}
                                value={formData.location}
                                placeholder="Location"
                                required
                                type="text"
                                name="location"
                                id="location"
                                className="form-control"
                                readOnly={action === 'delete'}
                            />
                            <label htmlFor="location">Location</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                onChange={handleChange}
                                value={formData.image}
                                placeholder="Image"
                                required
                                type="text"
                                name="image"
                                id="image"
                                className="form-control"
                                readOnly={action === 'delete'}
                            />
                            <label htmlFor="image">Image</label>
                        </div>
                        <div className="d-grid gap-2 col-12 mx-auto">
                            <button className="btn btn-success">
                                <b>
                                    {action.charAt(0).toUpperCase() +
                                        action.slice(1)}{' '}
                                    Sighting
                                </b>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default SightingForm
