import { useState } from 'react'
import { Modal } from 'react-bootstrap'
import Button from 'react-bootstrap/Button'
import { useGetTokenQuery } from '../api/birddexApi'
import { useSelector, useDispatch } from 'react-redux'
import { setSelectedCard } from '../selectedCard/selectedCardSlice'
import {
    setListPosition,
    setListItems,
} from '../listPosition/listPositionSlice'
import SightingForm from '../SightingForm'
import '../../styles/Card.css'

function Card(props) {
    // get the user token
    const { data: token, isLoading: tokenLoading } = useGetTokenQuery()
    // visibility state of the add sighting modal
    const [addSighting, setAddSighting] = useState(false)
    // the currently selected card state
    const selectedCard = useSelector((state) => state.selectedCard.value)
    // the current list state
    const listSet = useSelector((state) => state.listPosition.value)
    const dispatch = useDispatch()

    // populate and place the list of sightings for the selected bird
    const showSightings = () => {
        // set or unset selected card status
        if (selectedCard.id !== props.info.id) {
            dispatch(setSelectedCard(props.info.id))
        } else {
            dispatch(setSelectedCard(-1))
        }

        // set or unset list position and content
        if (listSet.row !== props.rowNum) {
            dispatch(setListPosition(props.rowNum))
            dispatch(
                setListItems({
                    sightings: [...props.info.sightings],
                    bird: {
                        image: props.info.image,
                        common_name: props.info.common_name,
                        id: props.info.id,
                    },
                })
            )
        } else {
            if (selectedCard.id !== props.info.id) {
                dispatch(setListPosition(props.rowNum))
                dispatch(
                    setListItems({
                        sightings: [...props.info.sightings],
                        bird: {
                            image: props.info.image,
                            common_name: props.info.common_name,
                        },
                    })
                )
            } else {
                dispatch(setListPosition(-1))
            }
        }
    }

    // toggle the add sighting modal
    const toggle = () => {
        setAddSighting(!addSighting)
    }

    return (
        <>
            <div
                className={
                    selectedCard.id === props.info.id
                        ? 'card-container-selected card-container mx-auto my-2'
                        : 'card-container mx-auto my-2'
                }
                style={{ width: '17.5rem', height: '20.625rem' }}
            >
                <div className="card">
                    <div className="card-background">
                        <div className="image-container">
                            <img
                                src={props.info.image + '?w=225'}
                                className="card-img-top"
                                loading="lazy"
                                alt={props.info.common_name}
                                style={{
                                    objectFit: 'cover',
                                    objectPosition: '50% 25%',
                                    height: '10rem',
                                }}
                            />
                        </div>
                        <div className="card-body">
                            <h5 className="card-title">
                                {props.info.common_name}
                            </h5>
                            <div>
                                <ul className="list-group list-group-flush">
                                    <li className="list-group-item">
                                        <strong>Scientific Name: </strong>
                                        {props.info.sci_name}
                                    </li>
                                    <li className="list-group-item">
                                        <strong>Status: </strong>
                                        {props.info.conservation_status}
                                    </li>
                                </ul>
                            </div>
                            <div className="buttons">
                                {!tokenLoading && token && (
                                    <>
                                        <Button
                                            type="button"
                                            className="addButton"
                                            onClick={() => setAddSighting(true)}
                                        >
                                            Add Sighting
                                        </Button>
                                        <Button
                                            type="button"
                                            className={
                                                props.info.sightings &&
                                                props.info.sightings.length
                                                    ? 'showButtonHasSighting showButton'
                                                    : 'showDeadButton'
                                            }
                                            onClick={
                                                props.info.sightings &&
                                                props.info.sightings.length
                                                    ? showSightings
                                                    : null
                                            }
                                        >
                                            Show Sighting
                                        </Button>
                                    </>
                                )}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Modal
                size="md"
                show={addSighting}
                onHide={() => setAddSighting(false)}
                backdrop="static"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="example-modal-sizes-title-sm">
                        New {props.info.common_name} sighting
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <SightingForm
                        action="create"
                        birdInfo={props.info}
                        toggle={toggle}
                    />
                </Modal.Body>
            </Modal>
        </>
    )
}

export default Card
