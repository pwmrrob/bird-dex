import { useGetBirdsQuery } from '../Components/api/birddexApi'
import Carousel from 'react-bootstrap/Carousel'

function RBCarousel(props) {
    const {
        data: birds,
        isLoading: birdsLoading,
    } = useGetBirdsQuery()

    const birdsCarousel = !(props.info.length > 0)
        ? !birdsLoading
            ? birds.slice(0, 3)
            : []
        : props.info
    return (
        <Carousel>
            {birdsCarousel.map((bird, index) => (
                <Carousel.Item key={index} interval={5000}>
                    <div
                        className="overlay-container"
                        style={{
                            height: '30rem',
                        }}
                    >
                        <img
                            src={bird.image + '?w=400'}
                            alt={`Bird ${index + 1}`}
                            className="w-100 h-100 object-fit-cover object-position-center shadow p-2"
                        />
                        <div
                            className="overlay position-absolute top-0 start-0 w-100 h-100"
                            style={{ backgroundColor: 'rgba(0, 0, 0, 0.3)' }}
                        ></div>
                        <Carousel.Caption className="position-absolute bottom-0 start-0 end-0 pb-5 text-white text-center">
                            <h2>
                                {bird.common_name
                                    ? bird.common_name
                                    : bird.bird.common_name}
                            </h2>
                            <p>
                                <u>Scientific Name:</u>{' '}
                                {bird.sci_name
                                    ? bird.sci_name
                                    : bird.bird.sci_name}
                            </p>
                            {bird.location ? (
                                <p>
                                    <u>Location:</u> {bird.location}
                                </p>
                            ) : (
                                ''
                            )}
                        </Carousel.Caption>
                    </div>
                </Carousel.Item>
            ))}
        </Carousel>
    )
}

export default RBCarousel
