import { useState, useEffect } from 'react'
import { useDispatch } from 'react-redux'
import { setListInitialState } from '../listPosition/listPositionSlice'
import { setSelectedInitialState } from '../selectedCard/selectedCardSlice'

function getWindowDimensions() {
    const { innerWidth: width } = window
    if (width > 1400) {
        return 4
    } else if (width > 992) {
        return 3
    } else if (width > 768) {
        return 2
    } else {
        return 1
    }
}

export default function useWindowDimensions() {
    const dispatch = useDispatch()

    const [windowDimensions, setWindowDimensions] = useState(
        getWindowDimensions()
    )

    useEffect(() => {
        function handleResize() {
            dispatch(setListInitialState())
            dispatch(setSelectedInitialState())
            setWindowDimensions(getWindowDimensions())
        }

        window.addEventListener('resize', handleResize)
        return () => window.removeEventListener('resize', handleResize)
    }, [
        dispatch,
        setWindowDimensions,
    ])

    return windowDimensions
}
