import { configureStore } from '@reduxjs/toolkit'
import { setupListeners } from '@reduxjs/toolkit/query'
import { birdsApi } from './Components/api/birddexApi'
import listPositionReducer from './Components/listPosition/listPositionSlice'
import selectedCardReducer from './Components/selectedCard/selectedCardSlice'

export const store = configureStore({
    reducer: {
        listPosition: listPositionReducer,
        selectedCard: selectedCardReducer,
        [birdsApi.reducerPath]: birdsApi.reducer,
    },
    middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware().concat(birdsApi.middleware),
})

setupListeners(store.dispatch)
