import RBCarousel from '../Components/RBCarousel'
import {
    useGetTokenQuery,
    useLazyGetSightingsQuery,
    useLazyGetObservationsQuery,
} from '../Components/api/birddexApi'
import { useState, useEffect } from 'react'
import Placeholder from '../Components/dashboard/Placeholder'
import SightingsBox from '../Components/dashboard/SightingsBox'


function Dashboard() {
    // holds any active error message
    const [err, setErr] = useState(null)
    // loads the active user's credentials
    const {
        data: user,
        error: userError,
        isLoading: userLoading,
    } = useGetTokenQuery()
    // fetch function for user's sightings
    const [
        fetchSightings,
        { data: sightings, error: sightingsError, isLoading: sightingsLoading },
    ] = useLazyGetSightingsQuery()

    // Query nearby observations API
    const [
        fetchObservations,
        {
            data: observations,
            error: observationsError,
            isLoading: observationsLoading,
        },
    ] = useLazyGetObservationsQuery()


    // if the user is logged in and validated, fetch their sightings,
    // if error, set the error message
    useEffect(() => {
        if (!userLoading && user !== null) {
            fetchSightings(user.user.id)
            fetchObservations()
        }
        if (userError) {
            setErr(userError)
        }
    }, [user, userError, userLoading, fetchSightings, fetchObservations])

    // if the user is logged in and validated, render the Dashboard,
    // otherwise render a landing page directing them to '/'
    if (userLoading || user === null)
        return <Placeholder err={err} />

    return (
        <div className="dashboard">
            <div id="title" className="d-flex">
                <h1 className="py-3">
                    Welcome {user.user.first_name}!
                </h1>
            </div>
            <div className="row dashrow mb-5">
                <div className="col-md">
                    <h3 className="py-2">Birds Seen</h3>
                    {err ? (
                        <div
                            className={
                                'alert alert-danger alert-dismissible'
                            }
                            role="alert"
                        >
                            {err}
                        </div>
                        ) : null
                    }
                    {sightingsLoading ? (
                            <p>Loading...</p>
                        ) : sightingsError ? (
                            setErr(sightingsError)
                        ) : sightings ? (
                            <RBCarousel info={sightings} />
                        ) : null
                    }
                </div>
                <div className="col-md">
                    <h3 className="py-2">Nearby Observations</h3>
                    {err ? (
                        <div
                            className={
                                'alert alert-danger alert-dismissible'
                            }
                            role="alert"
                        >
                            {err}
                        </div>
                        ) : null
                    }
                    {observationsLoading ? (
                            <p>Loading...</p>
                        ) : observationsError ? (
                            setErr(observationsError)
                        ) : observations ? (
                            <RBCarousel info={observations} />
                        ) : null
                    }
                </div>
            </div>
            {sightingsLoading ? (
                    <p>Loading...</p>
                ) : sightingsError ? (
                    setErr(sightingsError)
                ) : (
                    <SightingsBox sightings={sightings} />
                )
            }
        </div>
    )
}

export default Dashboard
